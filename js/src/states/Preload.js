
/* This is preload state to load all the assets */

TanksGameWrapper.Preload = function (game) {};

TanksGameWrapper.Preload.prototype.preload = function () {

  this.load.image("background", "images/background.png");
  this.load.image("bullet", "images/bullet.png");
  this.load.image("flame", "images/flame.png");
  this.load.image("land", "images/land.png");
  this.load.image("tank", "images/tank.png");
  this.load.image("target", "images/target.png");
  this.load.image("turret", "images/turret.png");

};
TanksGameWrapper.Preload.prototype.create = function () {

  console.log("hello from PRELOAD");
  game.state.start("MainMenu");
};