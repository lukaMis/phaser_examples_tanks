
/*

Tanks 0.1
Autor: Luka Mis

*/

// this is import point of app

//@prepros-prepend states/Boot.js
//@prepros-prepend states/Preload.js
//@prepros-prepend states/MainMenu.js
//@prepros-prepend states/Game.js

var game = new Phaser.Game(640, 480, Phaser.CANVAS, "game-container");

game.state.add("Boot", TanksGameWrapper.Boot);
game.state.add("Preload", TanksGameWrapper.Preload);
game.state.add("MainMenu", TanksGameWrapper.MainMenu);
game.state.add("Game", TanksGameWrapper.Game);

game.state.start("Boot");